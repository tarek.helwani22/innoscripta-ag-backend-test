<?php

// app/Services/GuardianService.php

namespace App\Services;

use App\Constants\NewsSources;
use App\Services\Interfaces\INewsService;
use Illuminate\Support\Facades\Http;

class GuardianService implements INewsService
{
    private string $endpoint;
    private string $apiKey;

    public function __construct()
    {
        $this->endpoint = env('THE_GUARDIAN_ENDPOINT');
        $this->apiKey = env('THE_GUARDIAN_API_KEY');
    }

    public function searchArticles(string $keyword, string $category = null, string $fromDate = null, int $page = 1): array
    {
        $queryParameters = [
            'api-key' => $this->apiKey,
            'q' => $keyword,
            'page-size' => 5,
            'page' => $page
        ];

        if ($category !== null) {
            $queryParameters['section'] = $this->mapCategory($category) ?? $category;
        }

        if ($fromDate !== null) {
            $queryParameters['from-date'] = $fromDate;
        }

        $response = Http::withOptions(['verify' => false])->get($this->endpoint, $queryParameters);

        $newsData = $response->json();

        return $this->mapToNewsItems($newsData['response']['results'] ?? []);
    }

    private function mapToNewsItems(array $newsData): array
    {
        $newsItems = [];

        foreach ($newsData as $item) {
            $newsItem = [
                'title' => $item['webTitle'],
                'description' => $item['fields']['trailText'] ?? '',
                'author' => $item['fields']['byline'] ?? '',
                'url' => $item['webUrl'],
                'source' => NewsSources::GUARDIAN,
                'category' => $item['sectionName'],
                'publishedAt' => $item['webPublicationDate'],
            ];

            $newsItems[] = $newsItem;
        }

        return $newsItems;
    }

    private function mapCategory(string $category): ?string
    {
        $mappings = config('CategoryMappings.guardian');
        return $mappings[$category] ?? null;
    }
}
