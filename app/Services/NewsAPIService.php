<?php

namespace App\Services;

use App\Constants\NewsSources;
use App\Services\Interfaces\INewsService;
use Illuminate\Support\Facades\Http;

class NewsAPIService implements INewsService
{
    private string $endpoint;
    private string $apiKey;

    public function __construct()
    {
        $this->endpoint = env('NEWSAPI_ENDPOINT');
        $this->apiKey = env('NEWSAPI_API_KEY');
    }

    public function searchArticles(string $keyword, string $category = null, string $fromDate = null, int $page = 1): array
    {
        $queryParameters = [
            'apiKey' => $this->apiKey,
            'q' => $keyword === "" ? 'news' : $keyword,
            'pageSize' => 5,
            $page => $page
        ];

        if ($category !== null) {
            $queryParameters['section'] = $this->mapCategory($category) ?? $category;
        }

        if ($fromDate !== null) {
            $queryParameters['from'] = $fromDate;
        }

        $response = Http::withOptions(['verify' => false])->get($this->endpoint, $queryParameters);

        $newsData = $response->json();

        return $this->mapToNewsItems($newsData['articles'] ?? []);
    }

    private function mapToNewsItems(array $newsData): array
    {
        $newsItems = [];

        foreach ($newsData as $item) {
            $newsItem = [
                'title' => $item['title'],
                'description' => $item['description'] ?? '',
                'author' => $item['author'] ?? '',
                'url' => $item['url'],
                'source' => NewsSources::NEWS_API,
                'category' => $item['category'] ?? '',
                'publishedAt' => $item['publishedAt'],
            ];

            $newsItems[] = $newsItem;
        }

        return $newsItems;
    }

    private function mapCategory(string $category): ?string
    {
        $mappings = config('CategoryMappings.newsapi');
        return $mappings[$category] ?? null;
    }
}
