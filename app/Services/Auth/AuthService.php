<?php

namespace App\Services\Auth;

use App\Dto\CreateUserDto;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthService
{
    public function createUser(CreateUserDto $userDTO): User
    {
        $user = new User();
        $user->name = $userDTO->name;
        $user->email = $userDTO->email;
        $user->password = Hash::make($userDTO->password);
        $user->save();

        return $user;
    }

    public function user(): \Illuminate\Contracts\Auth\Authenticatable
    {
        return Auth::user();
    }
}
