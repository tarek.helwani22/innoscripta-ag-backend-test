<?php

namespace App\Services\Interfaces;

interface INewsService
{
    public function searchArticles(string $keyword, string $category = null, string $fromDate = null, int $page = 1): array;
}
