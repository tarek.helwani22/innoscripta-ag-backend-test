<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Response;

class ResponseMacrosServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap services.
     */
    public function boot()
    {
        Response::macro('success', function ($data, $status = 200) {
            return Response::json(['data' => $data], $status);
        });

        Response::macro('error', function ($message, $status = 400) {
            return Response::json(['message' => $message], $status);
        });
    }
}
