<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GetNewsListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'keyword' => 'nullable|string',
            'page' => 'nullable|numeric|min:1',
            'sources' => ['nullable', 'array'],
            'sources.*' => 'string',
            'category' => ['nullable', 'string'],
            'fromDate' => 'nullable|date',
        ];
    }

}
