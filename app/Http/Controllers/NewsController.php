<?php

namespace App\Http\Controllers;

use App\Constants\NewsSources;
use App\Http\Requests\GetNewsListRequest;
use App\Services\GuardianService;
use App\Services\Interfaces\INewsService;
use App\Services\NewsAPIService;
use Illuminate\Http\Request;
use App\Models\Source;
use Illuminate\Support\Facades\Response;


class NewsController extends Controller
{
    private array $newsSources;

    public function __construct(private readonly GuardianService $guardianService,
                                private readonly NewsAPIService  $newsAPIService)
    {
        $this->newsSources = Source::pluck('identifier')->toArray();
    }

    public function index(GetNewsListRequest $request)
    {
        $keyword = $request->input('keyword') ?? '';
        $page = $request->input('page');
        $category = $request->input('category');
        $sources = $request->input('sources') ?? [];
        $fromDate = $request->input('fromDate');

        $selectedSources = array_intersect($this->newsSources, $sources);

        $news = [];

        foreach ($selectedSources as $selectedSource) {
            switch ($selectedSource) {
                case NewsSources::GUARDIAN:
                    $news = array_merge($news, $this->guardianService->searchArticles($keyword, $category, $fromDate, $page));
                    break;
                case NewsSources::NEWS_API:
                    $news = array_merge($news, $this->newsAPIService->searchArticles($keyword, $category, $fromDate, $page));
                    break;
            }
        }

        return Response::success($news);
    }
}
