<?php

namespace App\Http\Controllers;

use App\Models\Source;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class SourcesController extends Controller
{
    public function index(Request $request)
    {
        $sources = Source::all();
        return Response::success(['sources' => $sources]);
    }
}
