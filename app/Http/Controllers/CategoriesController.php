<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    public function index(Request $request)
    {
        $categories = Category::all();
        return Response::success(['categories' => $categories]);
    }
}
