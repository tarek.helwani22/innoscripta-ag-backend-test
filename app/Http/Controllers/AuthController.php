<?php

namespace App\Http\Controllers;

use App\Dto\CreateUserDto;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Services\Auth\AuthService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class AuthController extends Controller
{
    public function __construct(private readonly AuthService $authService)
    {
    }

    public function me()
    {
        $data = ['user' => $this->authService->user()->load(['category', 'sources'])];

        return Response::success($data);
    }

    public function register(RegisterRequest $request)
    {
        $userDTO = new CreateUserDTO(
            $request->input('name'),
            $request->input('email'),
            $request->input('password')
        );

        $user = $this->authService->createUser($userDTO);

        $token = $user->createToken($user->email)->plainTextToken;

        $data = [
            'user' => $user,
            'token' => $token,
        ];

        return Response::success($data);
    }

    public function login(LoginRequest $request)
    {
        $credentials = $request->only('email', 'password');
        if (!Auth::attempt($credentials)) {
            return Response::error('Invalid credentials', 401);
        }

        $user = Auth::user();

        $token = $user->createToken($user->email)->plainTextToken;

        $data = [
            'user' => $user,
            'token' => $token,
        ];

        return Response::success($data);
    }

    public function logout()
    {
        $user = Auth::user();
        $user->deleteToken();

        return Response::success([
            'message' => 'Logged Out Successfully'
        ]);
    }

}
