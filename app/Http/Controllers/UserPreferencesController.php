<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateUserPreferencesRequest;
use App\Models\Category;
use App\Models\Source;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class UserPreferencesController extends Controller
{
    public function index(UpdateUserPreferencesRequest $request) {
        $user = Auth::user();

        if ($request->has('category')) {
            $category = Category::where('identifier', $request->input('category'))->first();
            if ($category) {
                $user->category()->associate($category)->save();
            }
        } else {
            $user->category()->dissociate()->save();
        }

        if ($request->has('sources')) {
            $sourceIds = Source::whereIn('identifier', $request->input('sources'))->pluck('id');
            $user->sources()->sync($sourceIds);
        } else {
            $user->sources()->detach();
        }

        return Response::success(['message' => 'User preferences updated successfully']);
    }
}
