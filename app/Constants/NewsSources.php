<?php

namespace App\Constants;

class NewsSources
{
    public const GUARDIAN = 'guardian';
    public const NEWS_API = 'newsAPI';
}
