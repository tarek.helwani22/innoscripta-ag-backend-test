# News App

clone the project:

```shell
git clone https://gitlab.com/tarek.helwani22/innoscripta-ag-backend-test
```
install the packages:

```shell
composer install
```

run the project in development mode, first copy the .env file using the following command:
```shell
cp .env.example .env
```

run the migration then run the server
```shell
php artisan migrate:refresh --seed

php artisan serve
```

run the project using docker
```shell
docker compoes up
```
