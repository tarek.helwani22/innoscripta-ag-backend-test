<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SourceSeeder extends Seeder
{
    public function run()
    {
        DB::table('sources')->insert([
            ['identifier' => 'guardian', 'name' => 'The Guardian'],
            ['identifier' => 'newsAPI', 'name' => 'NewsAPI'],
        ]);
    }
}
