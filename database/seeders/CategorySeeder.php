<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('categories')->insert([
            ['identifier' => 'business', 'name' => 'Business'],
            ['identifier' => 'entertainment', 'name' => 'Entertainment'],
            ['identifier' => 'health', 'name' => 'Health'],
            ['identifier' => 'science', 'name' => 'Science'],
            ['identifier' => 'sport', 'name' => 'Sport'],
            ['identifier' => 'technology', 'name' => 'Technology'],
        ]);
    }
}
