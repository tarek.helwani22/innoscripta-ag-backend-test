<?php

return [
    'guardian' => [
        'business' => 'business',
        'entertainment' => 'games',
        'health' => 'healthcare-network',
        'science' => 'science',
        'sport' => 'sport',
        'technology' => 'technology'
        // Add more mappings for Guardian API categories
    ],
    'newsapi' => [
        'business' => 'business',
        'entertainment' => 'entertainment',
        'health' => 'health',
        'science' => 'science',
        'sport' => 'sports',
        'technology' => 'technology'
        // Add more mappings for NewsAPI categories
    ],
];
